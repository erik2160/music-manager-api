package university.jala.musicmanagerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MusicManagerApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MusicManagerApiApplication.class, args);
	}

}
