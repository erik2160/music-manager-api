package university.jala.musicmanagerapi.api.controller;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import university.jala.musicmanagerapi.domain.model.Music;
import university.jala.musicmanagerapi.domain.repository.MusicRepository;
import university.jala.musicmanagerapi.service.MusicService;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping("/musics")
public class MusicController {

    private final MusicService musicService;
    private final MusicRepository musicRepository;
    @GetMapping
    public List<Music> list() {
        return musicRepository.findAll();
    }

    @GetMapping("/id/{musicId}")
    public ResponseEntity<Music> find(@PathVariable Long musicId) {
        Optional<Music> music = musicRepository.findById(musicId);

        return music.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/title/{musicTitle}")
    public ResponseEntity<Iterable <Music>> findTitle(@PathVariable String musicTitle) {
        if (!musicRepository.existsByTitle(musicTitle)) {
            return ResponseEntity.notFound().build();
        }

        return new ResponseEntity<>(musicService.getMusicByTitle(musicTitle), HttpStatus.OK);
    }

    @GetMapping("/artist/{musicArtist}")
    public ResponseEntity<Iterable <Music>> findArtist(@PathVariable String musicArtist) {
        if (!musicRepository.existsByArtist(musicArtist)) {
            return ResponseEntity.notFound().build();
        }

        return new ResponseEntity<>(musicService.getMusicByArtist(musicArtist), HttpStatus.OK);
    }

    @GetMapping("/gender/{musicGender}")
    public ResponseEntity<Iterable <Music>> findGender(@PathVariable String musicGender) {
        if (!musicRepository.existsByGender(musicGender)) {
            return ResponseEntity.notFound().build();
        }

        return new ResponseEntity<>(musicService.getMusicByGender(musicGender), HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Music insert(@Valid @RequestBody Music music) {
        return musicService.save(music);
    }

    @DeleteMapping("/{musicId}")
    public ResponseEntity<Void> remove(@PathVariable Long musicId) {
        if (!musicRepository.existsById(musicId)) {
            return ResponseEntity.notFound().build();
        }

        musicService.delete(musicId);

        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{musicId}")
    public ResponseEntity<Music> update(@Valid @PathVariable Long musicId, @RequestBody Music music) {
        if (!musicRepository.existsById(musicId)) {
            return ResponseEntity.notFound().build();
        }
        music.setId(musicId);

        return ResponseEntity.ok(musicService.save(music));
    }
}
