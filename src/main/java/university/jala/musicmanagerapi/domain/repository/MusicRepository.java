package university.jala.musicmanagerapi.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import university.jala.musicmanagerapi.domain.model.Music;

@Repository
public interface MusicRepository extends JpaRepository<Music, Long> {
    boolean existsByGender(String gender);
    boolean existsByTitle(String title);
    boolean existsByArtist(String id);

    Iterable<Music> findByTitle(String title);
    Iterable<Music> findByArtist(String artist);
    Iterable<Music> findByGender(String artist);
}