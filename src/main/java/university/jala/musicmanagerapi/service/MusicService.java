package university.jala.musicmanagerapi.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import university.jala.musicmanagerapi.domain.model.Music;
import university.jala.musicmanagerapi.domain.repository.MusicRepository;

import java.util.Collections;

@Service
@AllArgsConstructor
public class MusicService {

    private final MusicRepository musicRepository;

    @Transactional
    public Music save(Music music) {
        return musicRepository.save(music);
    }

    @Transactional
    public void delete(Long musicId) {
        musicRepository.deleteById(musicId);
    }

    public Iterable<Music> getMusicByTitle(String title) {
        if (title.isEmpty()) {
            return Collections.emptyList();
        }

        return musicRepository.findByTitle(title);
    }

    public Iterable<Music> getMusicByArtist(String artist) {
        if (artist.isEmpty()) {
            return Collections.emptyList();
        }

        return musicRepository.findByArtist(artist);
    }

    public Iterable<Music> getMusicByGender(String gender) {
        if (gender.isEmpty()) {
            return Collections.emptyList();
        }

        return musicRepository.findByGender(gender);
    }
}
