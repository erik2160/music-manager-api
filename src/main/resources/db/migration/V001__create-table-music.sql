CREATE TABLE music (
    id bigint not null auto_increment,
    title varchar(255) not null,
    artist varchar(255) not null,
    gender varchar(255) not null,
    length varchar (8) not null,

    primary key (id)
);